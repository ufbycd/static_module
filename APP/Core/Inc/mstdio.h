/**
 * mstdio.h
 *
 *  Created on: 2019-09-05
 *      Author: chenshisheng
 */

#ifndef MSTDIO_H_
#define MSTDIO_H_

#include "main.h"
#include "stream/stream.h"
#include "message_queue.h"

void MStdio_Init(void);
void Mstdio_OnPrintf(const MsgQueue_Msg_t *msg);

#endif /* MSTDIO_H_ */
