/**
 * utils.h
 *
 *  Created on: 2017-3-21
 *      Author: chenshisheng
 */

#ifndef UTILS_H_
#define UTILS_H_

#include "main.h"
#include "timer.h"

#define UTILS_CRC16_INIT_VALUE 0x0000

#define Utils_DelayMs(ms) HAL_Delay(ms)

//void Utils_Init(void);
//void Utils_DelayMs(unsigned int ms);
Timer_Tick_t Utils_Time(void);
bool Utils_IsGlobalVariable(void *varAddr);
uint16_t Utils_Crc16Xmodem(uint16_t init, const void *data, unsigned int size);
uint16_t Utils_CRC16Modbus(const void *dataIn, unsigned int size);
uint8_t Utils_Crc8(const void *data, unsigned int size);
float Utils_Interpolate(float x0, float y0, float x1, float y1, float x);
int Utils_InterpolateInt(int x0, int y0, int x1, int y1, int x);

#endif /* UTILS_H_ */
