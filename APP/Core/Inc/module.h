/*
 * module.h
 *
 *  Created on: 2020-12-28
 *      Author: chenss
 */

#ifndef MODULE_H_
#define MODULE_H_

#include <stdint.h>
#include <stdarg.h>

typedef enum APP_Cmd
{
    APP_Cmd_GetSystemClock,
}APP_Cmd_t;

typedef struct
{
    int (*vprintf)(const char *fmt, va_list args);
    int (*puts)(const char *str);
    int (*ioctl)(unsigned int cmd, va_list args);
    void (*delay)(uint32_t ms);
}APP_Vector_t;

typedef struct
{
    int (*version_major)(void);
    int (*init)(void);
    int (*schedule)(void);
    int (*ioctl)(unsigned int cmd, va_list args);
}Module_Vector_t;

typedef void (*Module_ResetHandler_t)(const APP_Vector_t *app, Module_Vector_t *module);

#endif /* MODULE_H_ */
