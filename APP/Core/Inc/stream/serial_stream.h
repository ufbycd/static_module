/**
 * serial_stream.h
 *
 *  Created on: 2019-11-26
 *      Author: chenshisheng
 */

#ifndef STREAM_SERIAL_STREAM_H_
#define STREAM_SERIAL_STREAM_H_

#include "main.h"
#include "stream/io_stream.h"
#include "serial.h"

typedef IoStream_t SerialStream_t;

void SerialStream_Init(SerialStream_t *ss, Serial_t serial);

#endif /* STREAM_SERIAL_STREAM_H_ */
