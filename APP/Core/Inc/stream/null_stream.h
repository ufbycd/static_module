/**
 * null_stream.h
 *
 *  Created on: 2019-09-05
 *      Author: chenshisheng
 */

#ifndef STREAM_NULL_STREAM_H_
#define STREAM_NULL_STREAM_H_

#include "stream/stream.h"

typedef struct
{
    Stream_t stream;
}NullStream_t;

void NullStream_Init(NullStream_t *ns);

#endif /* STREAM_NULL_STREAM_H_ */
