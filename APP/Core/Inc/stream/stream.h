/**
 * stream.h
 *
 *  Created on: 2019-09-05
 *      Author: chenshisheng
 */

#ifndef STREAM_STREAM_H_
#define STREAM_STREAM_H_

#include "main.h"

#define Stream_MutexWait(a)
#define Stream_MutexRelease(a)
#define Stream_Delete(a)

typedef int (*Stream_Put_t)(void *instance, unsigned char b);
typedef int (*Stream_Get_t)(void *instance, unsigned char *pb);
typedef void (*Stream_Flush_t)(void *instance);

typedef struct
{
    Stream_Put_t put;
    Stream_Get_t get;
    Stream_Flush_t flush;
//    osMutexId mutexId;
}Stream_t;

void Stream_Init(Stream_t *stream, Stream_Put_t put, Stream_Get_t get, Stream_Flush_t flush);
void Stream_InitWithoutMutex(Stream_t *stream, Stream_Put_t put, Stream_Get_t get, Stream_Flush_t flush);

#if 0
static inline osStatus Stream_MutexWait(void *instance)
{
    Stream_t *stream = instance;

    if(stream->mutexId == NULL)
    {
        return osOK;
    }

    return osMutexWait(stream->mutexId, osWaitForever);
}

static inline osStatus Stream_MutexRelease(void *instance)
{
    Stream_t *stream = instance;

    if(stream->mutexId == NULL)
    {
        return osOK;
    }

    return osMutexRelease(stream->mutexId);
}

static inline void Stream_Delete(Stream_t *stream)
{
    if(stream->mutexId)
    {
        osMutexDelete(stream->mutexId);
    }
}
#endif

static inline int Stream_Put(void *instance, unsigned char b)
{
    Stream_t *stream = instance;

    return stream->put(instance, b);
}

static inline int Stream_Get(void *instance, unsigned char *pb)
{
    Stream_t *stream = instance;

    return stream->get(instance, pb);
}

int Stream_Write(void *instance, const void *data, unsigned int size);
int Stream_Read(void *instance, void *data, unsigned int size);
int Stream_ReadUntil(void *instance, char buf[], unsigned int maxSize, char till);
void Stream_Flush(void *instance);

#endif /* STREAM_STREAM_H_ */
