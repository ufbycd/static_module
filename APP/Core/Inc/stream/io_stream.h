/**
 * io_stream.h
 *
 *  Created on: 2019-09-05
 *      Author: chenshisheng
 */

#ifndef STREAM_IO_STREAM_H_
#define STREAM_IO_STREAM_H_

#include "main.h"
#include "ringbuffer.h"
#include "stream/stream.h"

typedef struct
{
    Stream_t stream;
    rbuf_t rbuf;
    const Stream_t *bridge;
    int userData;
}IoStream_t;

void IoStream_Init(
        IoStream_t *is,
        void *buf,
        unsigned int size,
        Stream_Put_t put,
        int userData);

void IoStream_InitWithoutBuffer(IoStream_t *is, Stream_Put_t put, Stream_Get_t get, int userData);
int IoStream_OnRx(IoStream_t *is, unsigned char b);
int IoStream_UserData(IoStream_t *is);

#endif /* STREAM_IO_STREAM_H_ */
