/**
 * mem_stream.h
 *
 *  Created on: 2019-09-05
 *      Author: chenshisheng
 */

#ifndef STREAM_MEM_STREAM_H_
#define STREAM_MEM_STREAM_H_

#include "stream/stream.h"

typedef struct
{
    Stream_t stream;
    unsigned char *buffer;
    unsigned int size;
    unsigned int eos;
    unsigned int offset;
}MemStream_t;

void MemStream_Init(MemStream_t *ms, void *buf, unsigned int size);
void MemStream_Reset(MemStream_t *ms);
unsigned int MemStream_Space(MemStream_t *ms);
void *MemStream_Data(MemStream_t *ms);
unsigned int MemStream_DataSize(MemStream_t *ms);

#endif /* STREAM_MEM_STREAM_H_ */
