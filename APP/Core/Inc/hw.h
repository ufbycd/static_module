/**
 * hw.h
 *
 *  Created on: 2018-05-11
 *      Author: chenshisheng
 */

#ifndef HW_H_
#define HW_H_

#include "main.h"
#include "message_queue.h"

typedef enum
{
    HW_Level_Low = RESET,
    HW_Level_High = SET
}HW_Level_t;

typedef enum
{
    HW_State_Inactive = DISABLE,
    HW_State_Active =  ENABLE
}HW_State_t;

typedef enum
{
    HW_In_User,
}HW_In_t;

typedef enum
{
    HW_Out_Run,
}HW_Out_t;

typedef enum
{
    HW_Exti_User,
}HW_Exti_t;

typedef void (*HW_ExtiCallback_t)(int data);

void HW_Init(void);
void HW_DeInit(void);
void HW_SetExtiCallback(HW_Exti_t exti, HW_ExtiCallback_t cb, int data);
void HW_ExtiCmd(HW_Exti_t exti, FunctionalState state);
void HW_OnExtiMsg(MsgQueue_Msg_t *msg);

void HW_Set(HW_Out_t out, HW_Level_t level);
HW_Level_t HW_GetSetted(HW_Out_t out);
void HW_Config(HW_Out_t out, HW_State_t status);
HW_State_t HW_IsConfigedActive(HW_Out_t out);
void HW_Toggle(HW_Out_t out);

HW_Level_t HW_Get(HW_In_t in);
bool HW_IsActive(HW_In_t in);
bool HW_IsExtiActive(HW_Exti_t exti);

unsigned int HW_ReadBoardAddres(void);
//void HW_OnExtiMsg(MsgQueue_Msg_t *msg);


typedef struct
{
    GPIO_TypeDef *port;
    uint32_t ll_pin;
}HW_t;

#define HW_MODE_OUTPUT 0
#define HW_MODE_INPUT  1

typedef enum
{
	HW_Mode_OutputPushPull,
	HW_Mode_OutputOpenDrain,
	HW_Mode_InputFlow,
	HW_Mode_InputPullUp,
	HW_Mode_InputPullDown
}HW_Mode_t;

#define HW_STRUCT(io) {_PORT(io), _PIN(io)}
#define HW_PIN_INIT(hw, HW_Mode) HW_GPIO_Init((hw)->port, (hw)->ll_pin, HW_Mode)

#define HW_SET(hw)  LL_GPIO_SetOutputPin((hw)->port, (hw)->ll_pin)
#define HW_CLR(hw)  LL_GPIO_ResetOutputPin((hw)->port, (hw)->ll_pin)
#define HW_GET(hw)  LL_GPIO_IsInputPinSet((hw)->port, (hw)->ll_pin)
#define HW_OUT(hw)  LL_GPIO_SetPinMode((hw)->port, (hw)->ll_pin, LL_GPIO_MODE_OUTPUT)
#define HW_IN(hw)   LL_GPIO_SetPinMode((hw)->port, (hw)->ll_pin, LL_GPIO_MODE_INPUT)

void HW_GPIO_Init(GPIO_TypeDef *port, uint32_t ll_pin, HW_Mode_t mode);

#endif /* HW_H_ */
