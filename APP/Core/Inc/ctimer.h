/**
 * ctimer.h
 *
 *  Created on: 2017-4-8
 *      Author: chenshisheng
 */

#ifndef CTIMER_H_
#define CTIMER_H_

#include "main.h"
#include "message_queue.h"


#define CTIMER_FLAG_ONCE   (0x01 << 2)
#define CTIMER_FLAG_URGEN  (0x01 << 3)

#define CTIMER_TICK_SECOND 1000
#define CTIMER_TICK_MINUTE (60 * CTIMER_TICK_SECOND)
#define CTIMER_TICK_HOUR   (60 * CTIMER_TICK_MINUTE)

typedef void (* Ctimer_Callback_t)(int data);

typedef struct Ctimer
{
    struct Ctimer *next; // for list
    unsigned int counter;
    unsigned int interval;
    unsigned int flags;
    int data;
    Ctimer_Callback_t callback;
}Ctimer_t;

void Ctimer_Init(void);
void Ctimer_DeInit(void);
void Ctimer_TickUpdate(unsigned int ms);
void Ctimer_Fire(MsgQueue_Msg_t *msg);

int Ctimer_Start(Ctimer_t *timer,
        unsigned int intervalMs,
        Ctimer_Callback_t cb,
        unsigned int flags,
        int data);
int Ctimer_Restart(Ctimer_t *timer, unsigned int intervalMs);
int Ctimer_Reset(Ctimer_t *timer);
int Ctimer_Stop(Ctimer_t *timer);
int Ctimer_IsRunning(Ctimer_t *timer);
void Ctimer_Pause(bool stat);
unsigned int Ctimer_MiniTick(void);

#endif /* CTIMER_H_ */
