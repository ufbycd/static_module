/**
 * watch_dog.h
 *
 *  Created on: 2018-03-09
 *      Author: chenshisheng
 */

#ifndef WATCH_DOG_H_
#define WATCH_DOG_H_

#include "main.h"

// 看门狗超时时长（秒）
#define WATCH_DOG_TIME_OUT 3

// 看门狗所需reload的定时时间间隔（秒）
#define WATCH_DOG_RELOAD_TIMING 2

void WatchDog_Init(void);
void WatchDog_Reload(void);

#endif /* WATCH_DOG_H_ */
