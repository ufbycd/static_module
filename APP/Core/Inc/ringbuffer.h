/**
 * ringbuffer.h
 *
 *  Created on: 2016-11-3
 *      Author: chenshisheng
 */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#include "main.h"


#define RINGBUFFER_FLAG_OVERWRITE (0x01)

typedef uint8_t elem_t;

/* Ring buffer object */
typedef struct
{
	unsigned int size;  /* maximum number of elements           */
	unsigned int start; /* index of oldest element              */
	unsigned int end;   /* index at which to write new element  */
	unsigned int mask;  /* mask for elem index                  */
	elem_t *elems;      /* vector of elements                   */
	unsigned int flags;
} rbuf_t;

void RingBuffer_Init(rbuf_t *cb, void *buf, unsigned int size);
void RingBuffer_SetFlags(rbuf_t *cb, unsigned int flags);
int8_t RingBuffer_IsFull(rbuf_t *cb);
int8_t RingBuffer_IsEmpyt(rbuf_t *cb);
int8_t RingBuffer_Write(rbuf_t *cb, void *e);
int8_t RingBuffer_Read(rbuf_t *cb, void *e);
void RingBuffer_Clear(rbuf_t *cb);

#endif /* RINGBUFFER_H_ */
