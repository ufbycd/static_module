/**
 * message_queue.h
 *
 *  Created on: 2017年4月6日
 *      Author: chenshisheng
 */

#ifndef MESSAGE_QUEUE_H_
#define MESSAGE_QUEUE_H_

#include "main.h"

// 单个消息的最大长度
#define MSGQUEUE_MSG_MAX_SIZE  128

// 消息标识
typedef enum
{
    MsgQueue_Id_ShellLineIn = 0,
    MsgQueue_Id_CtimerFire,
    MsgQueue_Id_Callback,
    MsgQueue_Id_SerialTest,
    MsgQueue_Id_ExtiTest,
	MsgQueue_Id_WatchDog,
	MsgQueue_Id_Printf,
	MsgQueue_Id_Jump,
}MsgQueue_Id_t;

// 消息结构
typedef struct
{
    int id;  // 标识
    unsigned int size; // 数据长度
    uint8_t data[MSGQUEUE_MSG_MAX_SIZE]; // 数据
}MsgQueue_Msg_t;

typedef void (* MsgQueue_Notify_t)(MsgQueue_Msg_t *msg);
typedef void (*MsgQueue_Callback_t)(void);

void MsgQueue_Init(MsgQueue_Notify_t globalNotify);
int MsgQueue_Send(int id, const void *dataIn, unsigned int size);
int MsgQueue_Recv(MsgQueue_Msg_t *msgOut);
void MsgQueue_Service(void);

static inline void MsgQueue_CallLater(MsgQueue_Callback_t callback)
{
    MsgQueue_Send(MsgQueue_Id_Callback, &callback, sizeof(callback));
}


#endif /* MESSAGE_QUEUE_H_ */
