/**
 * shell.h
 *
 *  Created on: 2016-11-23
 *      Author: chenshisheng
 */

#ifndef MAIN_SHELL_H_
#define MAIN_SHELL_H_

#include "main.h"
#include "stream/stream.h"
#include "message_queue.h"

#include <stdlib.h>

#define shell_declare(name) \
    static volatile const Shell_Cmd_t _shell_cmds_##name \
    __attribute__((unused, section(".shell_cmds."#name)))

#define SHELL_CMD(name, cmd, brief, usage) \
    shell_declare(name) = {#name, cmd, brief, usage}


typedef int (* Shell_Callback_t)(void *stream, int argc, char *argv[]);

typedef struct
{
    const char *name;
    Shell_Callback_t callback;
    const char *brief;
    const char *usage;
}Shell_Cmd_t;

void Shell_Init(void);
void Shell_OnLine(const MsgQueue_Msg_t *msg);
void Shell_PrintHelp(void *stream, const char *cmdName);

void Shell_OnSerialSend(const MsgQueue_Msg_t *msg);

#endif /* MAIN_SHELL_H_ */
