/**
 * package_buffer.h
 *
 *  Created on: 2017年3月27日
 *      Author: chenshisheng
 */

#ifndef PACKAGE_BUFFER_H_
#define PACKAGE_BUFFER_H_

#include "ringbuffer.h"

typedef struct
{
    unsigned int start;
    unsigned int end;
    unsigned int size;
    unsigned int mask;
    unsigned int elemMaxSize;
    uint8_t  *buf;
}pbuf_t;

void PackageBuffer_Init(pbuf_t *pb, void *buf, unsigned int size, unsigned int elemMaxSize);
int PackageBuffer_Write(pbuf_t *pb, const void *pkgData, unsigned int pkgSize);
int PackageBuffer_Read(pbuf_t *pb, void *pkgData, unsigned int *pkgSize);
void PackageBuffer_Clear(pbuf_t *pb);
bool PackageBuffer_IsFull(pbuf_t *pb);

#endif /* PACKAGE_BUFFER_H_ */
