/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_iwdg.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_dma.h"

#include "stm32f4xx_ll_exti.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#include <sys/cdefs.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include "stream/stream.h"
#include "stream/chprintf.h"

#ifndef FALSE
#   define FALSE false
#endif

#ifndef TRUE
#   define TRUE true
#endif

#define snprintf chsnprintf

extern unsigned int __criticalNesting;

static inline void _EnterCritical(void)
{
    __disable_irq();
    __criticalNesting ++;
}

static inline void _ExitCritical(void)
{
    assert_param(__criticalNesting);
    __criticalNesting --;
    if(__criticalNesting == 0)
    {
        __enable_irq();
    }
}

#define ENTER_CRITICAL_SECTION() _EnterCritical()
#define EXIT_CRITICAL_SECTION()  _ExitCritical()

#ifdef DEBUG
#   define debug(fmt, args...) printf(fmt, ##args)
#   define vdebug(fmt, vargs)  vprintf(fmt, vargs)
#   define mdebug(fmt, args...) mprintf(fmt, ##args)
#   define ON_DEBUG(foo) foo
#   define ON_RELEASE(foo)
#   define IDLE() __WFI()
#   define __DEBUG_BKPT()  asm volatile ("bkpt 0")
#else
#   define debug(...)
#   define vdebug(fmt, vargs)
#   define mdebug(fmt, vargs...)
#   define ON_DEBUG(foo)
#   define ON_RELEASE(foo) foo
#   define IDLE() __WFI()
#   define __DEBUG_BKPT()
#endif

#ifndef UNUSED
#define UNUSED(x) (void) x
#endif

//#define MAX(a, b) ((a) > (b) ? (a) : (b))
//#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define ARRAY_LEN(a) (sizeof(a) / sizeof(a[0]))
#define BIT_SET(reg, bit) (reg) |= (bit)
#define BIT_CLR(reg, bit) (reg) &= ~ (bit)
#define BITS_SET(reg, mask, bits) do{reg = (reg & (~(mask))) | (bits);}while(0)
#define BIT_MASK(n) (0x01 << (n))

#define _PORT(p, n) p
#define _PIN(p, n)  n
#define PORT(io) (_PORT io)
#define PIN(io)  (_PIN io)
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
int mprintf(const char *fmt, ...);
bool Main_IsModuleLoaded(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
