/*
 * package_buffer.c
 *
 *  Created on: 2017年3月27日
 *      Author: chenshisheng
 */


#include "package_buffer.h"

/**
 * @brief 初始化队列结构
 * @param pb   队列结构指针
 * @param buf  队列缓存
 * @param size 缓存大小
 * @param elemMaxSize 存入队列的单个元素的最大大小
 * @note size 值必须为2的n次方
 */
void PackageBuffer_Init(pbuf_t *pb, void *buf, unsigned int size, unsigned int elemMaxSize)
{
    pb->start = 0;
    pb->end = 0;
    pb->size = size;
    pb->mask = 2 * pb->size - 1;
    pb->elemMaxSize = elemMaxSize;
    pb->buf = buf;
}

/// 缓存是否已满
#define _IS_FULL(pb, end) (end == (pb->start ^ pb->size))

/// 缓存是否是空的
#define _IS_EMPTY(pb) (pb->end == pb->start)

/// 指向队列缓存的索引递增
#define _INCR(pb, p) ((p + 1) & pb->mask)

bool PackageBuffer_IsFull(pbuf_t *pb)
{
    return _IS_FULL(pb, pb->end);
}

int PackageBuffer_Write(pbuf_t *pb, const void *dataIn, unsigned int pkgSize)
{
    int ret;
    const uint8_t *vdata;
    unsigned int end, i;

    vdata = dataIn;
    end = pb->end;
    ret = 0;

    if(pkgSize > pb->elemMaxSize)
    {
        debug("PBuf Error: pkg too large!\n");
        return -1;
    }

    if(_IS_FULL(pb, pb->end))
    {
        return -1;
    }

//    ENTER_CRITICAL_SECTION();

    pb->buf[end & (pb->size - 1)] = (uint8_t) pkgSize;
    end = _INCR(pb, end);

    for(i = 0; i < pkgSize; i++)
    {
        if(_IS_FULL(pb, end))
        {
            ret = -1;
            break;
        }

        pb->buf[end & (pb->size - 1)] = vdata[i];
        end = _INCR(pb, end);
    }

    if(ret == 0)
    {
        pb->end = end;
    }

//    EXIT_CRITICAL_SECTION();
    return ret;
}

int PackageBuffer_Read(pbuf_t *pb, void *dataOut, unsigned int *pkgSize)
{
    int8_t ret;
    uint8_t *vdata;
    unsigned int size, i;

    if(_IS_EMPTY(pb))
    {
        return -1;
    }

    ret = 0;
    vdata = dataOut;

    size = pb->buf[pb->start & (pb->size - 1)];
    if(pkgSize)
    {
        *pkgSize = size;
    }

    pb->start = _INCR(pb, pb->start);
    if(size > pb->elemMaxSize)
    {
        return -1;
    }

//    ENTER_CRITICAL_SECTION();
    for(i = 0; i < size; i ++)
    {
        if(_IS_EMPTY(pb))
        {
            ret = -1;
            break;
        }

        vdata[i] = pb->buf[pb->start & (pb->size - 1)];
        pb->start = _INCR(pb, pb->start);
    }

//    EXIT_CRITICAL_SECTION();
    return ret;
}

void PackageBuffer_Clear(pbuf_t *pb)
{
    pb->start = 0;
    pb->end = 0;
}
