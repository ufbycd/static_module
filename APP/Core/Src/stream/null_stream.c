/**
 * null_stream.c
 *
 *  Created on: 2019-09-05
 *      Author: chenshisheng
 */

#include "stream/null_stream.h"

static int _Put(void *instance, unsigned char b)
{
    UNUSED(instance);
    UNUSED(b);

    return 1;
}

static int _Get(void *instance, unsigned char *pb)
{
    UNUSED(instance);
    UNUSED(pb);

    return 1;
}

static void _Flush(void *instance)
{
    UNUSED(instance);
}

void NullStream_Init(NullStream_t *ns)
{
    Stream_Init(&ns->stream, _Put, _Get, _Flush);
}
