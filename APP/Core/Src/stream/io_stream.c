/**
 * io_stream.c
 *
 *  Created on: 2019-09-05
 *      Author: chenshisheng
 */

#include "stream/io_stream.h"

int IoStream_OnRx(IoStream_t *is, unsigned char b)
{
    return (RingBuffer_Write(& is->rbuf, & b) == 0) ? 1 : 0;
}

static int _Get(void *instance, unsigned char *pb)
{
    IoStream_t *is = instance;

    return (RingBuffer_Read(& is->rbuf, pb) == 0) ? 1 : 0;
}

static void _Flush(void *instance)
{
    UNUSED(instance);
}

void IoStream_Init(
        IoStream_t *is,
        void *buf,
        unsigned int size,
        Stream_Put_t put,
        int userData)
{
    Stream_Init(&is->stream, put, _Get, _Flush);
    is->userData = userData;

    RingBuffer_Init(& is->rbuf, buf, size);
}

void IoStream_InitWithoutBuffer(IoStream_t *is, Stream_Put_t put, Stream_Get_t get, int userData)
{
    Stream_Init(&is->stream, put, get, _Flush);
    is->userData = userData;

    RingBuffer_Init(& is->rbuf, NULL, 0);
}

int IoStream_UserData(IoStream_t *is)
{
    return is->userData;
}
