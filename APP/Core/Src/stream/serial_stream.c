/**
 * serial_stream.c
 *
 *  Created on: 2019-11-26
 *      Author: chenshisheng
 */

#include "stream/serial_stream.h"

static int _Put(void *instance, unsigned char b)
{
	SerialStream_t *ss = instance;
    Serial_t serial = (Serial_t)ss->userData;

    Serial_SendByte(serial, b);
    return 1;
}

static int _Get(void *instance, unsigned char *pb)
{
	SerialStream_t *ss = instance;
    Serial_t serial = (Serial_t)ss->userData;

    return (Serial_RecvByte(serial, pb) == 0) ? 1 : 0;
}

void SerialStream_Init(SerialStream_t *ss, Serial_t serial)
{
    IoStream_InitWithoutBuffer((IoStream_t *)ss, _Put, _Get, serial);
}
