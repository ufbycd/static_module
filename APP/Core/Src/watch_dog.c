/**
 * watch_dog.c
 *
 *  Created on: 2018-03-09
 *      Author: chenshisheng
 */

#include "watch_dog.h"
#include "ctimer.h"
#include "message_queue.h"

#define IWDGx IWDG

static Ctimer_t _timer;

static void _OnTimeout(int data)
{
	UNUSED(data);

	MsgQueue_Send(MsgQueue_Id_WatchDog, NULL, 0);
}

void WatchDog_Init(void)
{
    uint32_t reload;

    /* Check if the system has resumed from IWDG reset */
    if(LL_RCC_IsActiveFlag_IWDGRST())
    {
      printf("Watch Dog Reset!\n");
    }
    else if(LL_RCC_IsActiveFlag_SFTRST())
    {
        printf("Software Reset\n");
    }
    else if(LL_RCC_IsActiveFlag_PINRST())
    {
        printf("NRST Pin Reset!\n");
    }

    LL_RCC_ClearResetFlags();

    /* Enable IWDG (the LSI oscillator will be enabled by hardware) */
    LL_IWDG_Enable(IWDGx);
    /* Enable write access to IWDG_PR and IWDG_RLR registers */
    LL_IWDG_EnableWriteAccess(IWDGx);
    /* IWDG counter clock: LSI/128 */
    LL_IWDG_SetPrescaler(IWDGx, LL_IWDG_PRESCALER_256);

    /* Set counter reload value to obtain 3s IWDG TimeOut.
       Counter Reload Value = timeout / (prescaler / LSI)
                            = timeout * LSI / prescaler
     */
    reload = (uint32_t) (WATCH_DOG_TIME_OUT * LSI_VALUE / 256);
    assert_param(reload <= 0xfff);
    LL_IWDG_SetReloadCounter(IWDGx, reload);
    while (LL_IWDG_IsReady(IWDGx) != 1)
    {
    }

    /* Reload IWDG counter */
    LL_IWDG_ReloadCounter(IWDGx);
    Ctimer_Start(&_timer, WATCH_DOG_RELOAD_TIMING * 1000, _OnTimeout, 0, 0);
}

void WatchDog_Reload(void)
{
    LL_IWDG_ReloadCounter(IWDGx);
}
