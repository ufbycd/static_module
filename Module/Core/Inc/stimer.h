/*
 * stimer.h
 *
 *  Created on: 2020-12-31
 *      Author: chenss
 */

#ifndef INC_STIMER_H_
#define INC_STIMER_H_

#include "main.h"

#define STIMER_FREQ 10

#define STIMER_TICK_SECOND STIMER_FREQ

void STimer_Init(void);
uint32_t STimer_Tick(void);

#endif /* INC_STIMER_H_ */
